package com.example.bcnpatrol.viewModel

import android.content.ContentValues.TAG
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bcnpatrol.model.api.Repository
import com.example.bcnpatrol.model.classes.Incidence
import com.example.bcnpatrol.model.classes.IncidencePost
import com.example.bcnpatrol.model.classes.StatePut
import com.example.bcnpatrol.model.classes.UserRanking
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File

class IncidenceViewModel: ViewModel() {
    val data = MutableLiveData<List<Incidence>>()
    val rankingList = MutableLiveData<List<UserRanking>>()
    private var repository = Repository("")
    val currentIncidence = MutableLiveData<Incidence>()
    val isIncidenceAddedSuccessfully = MutableLiveData<Boolean?>()
    var isMapTheCurrentFragment = true
    val isIncidenceLoaded = MutableLiveData<Boolean?>()

    fun getAllIncidences() {
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getAllIncidences()
            if (response.isSuccessful) {
                data.postValue(response.body())
            }
            else {
                Log.d(TAG, response.message())
            }
        }
    }

    fun addIncidence(incidence: IncidencePost, image: File){
        val imagePart = MultipartBody.Part.createFormData("image", "image", image.asRequestBody("image/*".toMediaType()))

        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.addIncidence(
                incidence.name.toRequestBody(),
                incidence.description.toRequestBody(),
                incidence.lat.toRequestBody(),
                incidence.lng.toRequestBody(),
                incidence.reporter.toRequestBody(),
                incidence.category.toRequestBody(),
                imagePart
            )
            if (response.isSuccessful) {
                isIncidenceAddedSuccessfully.postValue(true)
                Log.d(TAG, "SUCCESSFUL POST: ${response.message()}")
            }
            else {
                isIncidenceAddedSuccessfully.postValue(false)
                Log.d(TAG, "ERROR: ${response.message()}")
            }
        }
    }

    fun changeState(id: Int, newState: StatePut){
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.changeState(id, newState)
            if (response.isSuccessful) {
                Log.d(TAG, "1234 ${response.message()}")
                repository.getSvgs()
            }
            else {
                Log.d(TAG, "Response body raw: ${response.raw()}\n" +
                        "Response body: ${response.body()}\n1234 no se hizo, ${response.message()}")
            }
        }
    }
    fun getIncidenceById(id: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getIncidenceById(id)
            if (response.isSuccessful) {
                currentIncidence.postValue(response.body()!!)
                isIncidenceLoaded.postValue(true)
            }
            else {
                isIncidenceLoaded.postValue(false)
                Log.d(TAG, response.message())
            }
        }
    }

    fun getRankingList() {
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getRanking()
            if (response.isSuccessful) {
                rankingList.postValue(response.body())
            }
            else {
                Log.d(TAG, response.message())
            }
        }
    }
}
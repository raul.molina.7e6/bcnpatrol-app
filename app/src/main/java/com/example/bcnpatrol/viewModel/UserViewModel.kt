package com.example.bcnpatrol.viewModel

import android.content.ContentValues.TAG
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bcnpatrol.model.api.Repository
import com.example.bcnpatrol.model.classes.Email
import com.example.bcnpatrol.model.classes.Incidence
import com.example.bcnpatrol.model.classes.User
import com.example.bcnpatrol.model.classes.UserLoginAuth
import com.example.bcnpatrol.model.classes.UserPut
import com.example.bcnpatrol.model.classes.UserRegisterAuth
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File

class UserViewModel: ViewModel() {
    lateinit var actualUser: User
    val data = MutableLiveData<List<Incidence>>()
    var repository = Repository("")
    val categories = MutableLiveData<List<String>?>()
    var isLoginSuccessful = MutableLiveData<Boolean?>()
    var isUserDataChangedSuccessfully = MutableLiveData<Boolean>()

    fun getUserIncidences() {
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getUserIncidences(actualUser.email)
            if (response.isSuccessful) {
                data.postValue(response.body())
            }
            else {
                Log.d(TAG, response.message())
            }
        }
    }

    fun login(user: UserLoginAuth) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.login(user)
            if (response.isSuccessful) {
                isLoginSuccessful.postValue(true)
                actualUser = response.body()!!.user
                repository = Repository(response.body()!!.authorization.token)
                Log.d(TAG, "LOGIN SUCCESSFUL!")
            }
            else {
                isLoginSuccessful.postValue(false)
                Log.d(TAG, "LOGIN NOT SUCCESSFUL!")
            }
        }
    }

    fun register(userAuth: UserRegisterAuth) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.register(userAuth)
            if (response.isSuccessful) {
                isLoginSuccessful.postValue(true)
                actualUser = response.body()!!.user
                repository = Repository(response.body()!!.authorization.token)
                Log.d(TAG, "LOGIN SUCCESSFUL!")
            }
            else {
                isLoginSuccessful.postValue(false)
                Log.d(TAG, "LOGIN NOT SUCCESSFUL!")
            }
        }
    }

    fun getCategoriesForSpinner(){
        CoroutineScope(Dispatchers.IO).launch {
            val lista = repository.getCategoriesForSpinner().body() as List<String>
            categories.postValue(lista)
        }
    }

    fun recoverPassword(email: Email) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.recoverPassword(email)
            if (response.isSuccessful) {
                Log.d(TAG, "RECOVER PASSWORD SUCCESSFUL!")
            }
            else {
                Log.d(TAG, "RECOVER PASSWORD NOT SUCCESSFUL!")
            }
        }
    }

    fun modifyUserData(user: UserPut) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.changeUserEmail(actualUser.email, user)
            if (response.isSuccessful) {
                isUserDataChangedSuccessfully.postValue(true)
                Log.d(TAG, "PUT SUCCESSFUL!, ${response.message()}")
                isUserDataChangedSuccessfully.postValue(false)
            }
            else {
                Log.d(TAG, "PUT NOT SUCCESSFUL!, ${response.message()}")
            }
        }
    }

    fun modifyUserImage(image: File) {
        val imagePart = MultipartBody.Part.createFormData("image", "image", image.asRequestBody("image/*".toMediaType()))
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.modifyUserImage(actualUser.email, imagePart)
            if (response.isSuccessful) {
                val user = Gson().fromJson(response.body()!!.string(), User::class.java)
                actualUser.image = user.image
                Log.d(TAG, "USER IMAGE MODIFIED SUCCESSFULLY!")
            }
            else {
                Log.d(TAG, "USER IMAGE NOT MODIFIED!")
            }
        }
    }
}
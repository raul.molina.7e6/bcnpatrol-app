package com.example.bcnpatrol.view.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.bcnpatrol.model.classes.UserLoginAuth
import com.example.bcnpatrol.R
import com.example.bcnpatrol.view.activities.MainActivity
import com.example.bcnpatrol.viewModel.UserViewModel
import com.example.bcnpatrol.databinding.FragmentLoginBinding
import java.net.ConnectException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.util.concurrent.TimeoutException

class LoginFragment : Fragment() {

    private lateinit var binding: FragmentLoginBinding
    private lateinit var userViewmodel: UserViewModel
    private lateinit var myPreferences: SharedPreferences

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        userViewmodel = ViewModelProvider(requireActivity())[UserViewModel::class.java]
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(false)

        myPreferences = requireActivity().getSharedPreferences("MySharedPreferences", Context.MODE_PRIVATE)
        setupForm()

        binding.loginButton.setOnClickListener {
            changeViewAsCharging(true)
            val userEmail = binding.userEmailEt.text.toString()
            val userPassword = binding.userPasswordEt.text.toString()

            if (userEmail.isEmpty() || userPassword.isEmpty()) {
                if (userEmail.isEmpty()) binding.userEmailEt.error = "El correo electronico no puede estar vacío"
                if (userPassword.isEmpty()) binding.userPasswordEt.error = "La contraseña no puede estar vacía"
                return@setOnClickListener
            }
            else {
                binding.userEmailEt.error = null
                binding.passwordTv.error = null
            }

            if(!userEmail.contains('@') || !userEmail.contains('.')) {
                binding.userEmailEt.error = "Formato inválido de correo electronico"
                return@setOnClickListener
            }
            else {
                binding.userEmailEt.error = null
            }
            login(userEmail, userPassword)
        }

        binding.createNewAccountButton.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_signUpFragment)
        }

        binding.notRememberPasswordButton.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_recoverPasswordFragment)
        }

        userViewmodel.isLoginSuccessful.observe(viewLifecycleOwner) {
            if (it == true) {
                changeViewAsCharging(false)
                rememberUser(binding.userEmailEt.text.toString(), binding.userPasswordEt.text.toString(), binding.rememberUserSwitch.isChecked)
                showSuccessfullLoginToast()
                findNavController().navigate(R.id.action_loginFragment_to_incidentsListFragment)
            }
            else if (it == false) {
                showInvalidCredentialsToast()
            }
        }
    }

    private fun login(email: String, password: String) {
        val userAuth = UserLoginAuth(email, password)

        try {
            userViewmodel.login(userAuth)
        } catch (e: TimeoutException) {
            showServerErrorToast()
        } catch (e: ConnectException) {
            showServerErrorToast()
        } catch (e: SocketException) {
            showServerErrorToast()
        } catch (e: SocketTimeoutException) {
            showServerErrorToast()
        }
    }

    private fun rememberUser(email: String, pass: String, remember: Boolean) {
        if(remember){
            myPreferences.edit {
                putString("email", email)
                putString("password", pass)
                putBoolean("remember", remember)
                apply()
            }
        }else{
            myPreferences.edit {
                putString("email", "")
                putString("password", "")
                putBoolean("remember", remember)
                apply()
            }
        }
    }
    private fun setupForm() {
        val email = myPreferences.getString("email", "")
        val pass = myPreferences.getString("password", "")
        val remember = myPreferences.getBoolean("remember", false)
        if (email != null) {
            if(email.isNotEmpty()){
                binding.userEmailEt.setText(email)
                binding.userPasswordEt.setText(pass)
                binding.rememberUserSwitch.isChecked = remember
            }
        }
    }

    @SuppressLint("InflateParams")
    private fun showSuccessfullLoginToast() {
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.custom_toast_ok, null)
        val toast = Toast(context)
        toast.duration = Toast.LENGTH_SHORT
        toast.view = layout
        toast.show()
    }

    @SuppressLint("InflateParams")
    private fun showInvalidCredentialsToast() {
        changeViewAsCharging(false)
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.custom_toast_error, null)
        val toast = Toast(context)
        toast.duration = Toast.LENGTH_SHORT
        toast.view = layout
        toast.show()

    }

    @SuppressLint("InflateParams")
    private fun showServerErrorToast() {
        changeViewAsCharging(false)
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.custom_toast_general_error, null)
        val toast = Toast(context)
        toast.duration = Toast.LENGTH_SHORT
        toast.view = layout
        toast.show()
    }

    @SuppressLint("Range")
    fun changeViewAsCharging(charging : Boolean){
        val loadingArrow = binding.loadingArrow
        if(charging){
            binding.loginButton.isClickable = false
            binding.createNewAccountButton.isClickable = false
            binding.notRememberPasswordButton.isClickable = false
            binding.rememberUserSwitch.isClickable = false

            loadingArrow.visibility = View.VISIBLE
            loadingArrow.startAnimation(AnimationUtils.loadAnimation(requireContext(), com.example.bcnpatrol.R.anim.rotate))

        }else{
            /*
            binding.fragmentEntero.setBackgroundColor(Color.parseColor("#00000000"))
            binding.fragmentEntero.alpha = 100f
            * */
            binding.loginButton.isClickable = true
            binding.createNewAccountButton.isClickable = true
            binding.notRememberPasswordButton.isClickable = true
            binding.rememberUserSwitch.isClickable = true

            loadingArrow.clearAnimation()
            loadingArrow.visibility = View.INVISIBLE
        }
    }
}
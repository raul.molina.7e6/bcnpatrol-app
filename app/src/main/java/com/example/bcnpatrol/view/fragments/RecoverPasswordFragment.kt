package com.example.bcnpatrol.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.bcnpatrol.model.classes.Email
import com.example.bcnpatrol.R
import com.example.bcnpatrol.viewModel.UserViewModel
import com.example.bcnpatrol.databinding.FragmentRecoverPasswordBinding
import java.net.ConnectException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.util.concurrent.TimeoutException

class RecoverPasswordFragment : Fragment() {

    private lateinit var binding: FragmentRecoverPasswordBinding
    private lateinit var userViewmodel: UserViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        userViewmodel = ViewModelProvider(requireActivity())[UserViewModel::class.java]
        binding = FragmentRecoverPasswordBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.returnToLoginButton.setOnClickListener {
            findNavController().navigate(R.id.action_recoverPasswordFragment_to_loginFragment)
        }

        binding.recoverPasswordButton.setOnClickListener {
            val userEmail = binding.userEmailEt.text.toString()
            if(!userEmail.contains('@') || !userEmail.contains('.')) {
                binding.userEmailEt.error = "Formato inválido de correo electronico"
                return@setOnClickListener
            }

            try {
                userViewmodel.recoverPassword(Email(userEmail))
                showSuccessRecoverToast()
            } catch (e: TimeoutException) {
                showServerErrorToast()
            } catch (e: ConnectException) {
                showServerErrorToast()
            } catch (e: SocketException) {
                showServerErrorToast()
            } catch (e: SocketTimeoutException) {
                showServerErrorToast()
            }
        }
    }

    @SuppressLint("InflateParams")
    private fun showSuccessRecoverToast() {
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.custom_toast_recover_password, null)
        val toast = Toast(context)
        toast.duration = Toast.LENGTH_SHORT
        toast.view = layout
        toast.show()
    }

    @SuppressLint("InflateParams")
    private fun showServerErrorToast() {
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.custom_toast_general_error, null)
        val toast = Toast(context)
        toast.duration = Toast.LENGTH_SHORT
        toast.view = layout
        toast.show()
    }
}
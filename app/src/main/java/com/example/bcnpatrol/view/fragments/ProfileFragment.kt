package com.example.bcnpatrol.view.fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.ui.setupWithNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.bcnpatrol.model.classes.UserPut
import com.example.bcnpatrol.R
import com.example.bcnpatrol.view.activities.MainActivity
import com.example.bcnpatrol.viewModel.UserViewModel
import com.example.bcnpatrol.databinding.FragmentProfileBinding
import com.google.android.material.textfield.TextInputEditText
import java.io.File
import java.net.ConnectException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.util.concurrent.TimeoutException

class ProfileFragment : Fragment() {

    private lateinit var binding: FragmentProfileBinding
    private lateinit var userViewModel: UserViewModel
    private lateinit var userPut: UserPut
    private lateinit var imageUri: Uri
    private lateinit var mainActivity: MainActivity

    private var resultLauncherImage = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data: Intent? = result.data
            if (data != null) {
                imageUri = data.data!!
                userViewModel.modifyUserImage(getFileFromUri(requireContext(), imageUri)!!)
                Glide.with(requireContext())
                    .load(imageUri)
                    .transform(RoundedCorners(300))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(binding.userImageIv)
            }
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentProfileBinding.inflate(layoutInflater)
        userViewModel = ViewModelProvider(requireActivity())[UserViewModel::class.java]
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        try {
            val user = userViewModel.actualUser

            binding.usernameTv.text = user.name
            binding.useremailTv.text = user.email

            if (user.type == "1") {
                binding.userTypeTv.text = "Ciutadana"
            }
            else if(user.type == "2"){
                binding.userTypeTv.text = "Brigadista"
            }

            Glide.with(requireContext())
                .load("https://bcnpatrol-api.kirocs.es/${user.image}")
                .transform(RoundedCorners(300))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.userImageIv)

            if("https://bcnpatrol-api.kirocs.es/${user.image}" != "https://bcnpatrol-api.kirocs.es/storage/users/default.png"){
                binding.userImageIv.rotation = -90f
            }

            binding.changeUserNameIb.setOnClickListener {
                val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_user_changes, null)
                val builder = AlertDialog.Builder(context)
                builder.setTitle("Type your name...")
                val editText = dialogView.findViewById<TextInputEditText>(R.id.user_changed_et)
                builder.setView(dialogView)
                builder.setPositiveButton("OK") { _, _ ->
                    val newUserName = editText.text.toString()
                    if (newUserName != "") {
                        changeViewAsCharging(true)
                        userPut = UserPut(newUserName, userViewModel.actualUser.email, userViewModel.actualUser.type)
                        userViewModel.modifyUserData(userPut)
                        userViewModel.actualUser.name = newUserName
                        binding.usernameTv.text = newUserName
                    }
                }
                builder.setNegativeButton("Cancel") { dialogInterface, _ ->
                    dialogInterface.cancel()
                }
                builder.show()
            }

            binding.changeUserEmailIb.setOnClickListener {
                val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_user_changes, null)
                val builder = AlertDialog.Builder(context)
                builder.setTitle("Type your email...")
                val editText = dialogView.findViewById<TextInputEditText>(R.id.user_changed_et)
                builder.setView(dialogView)
                builder.setPositiveButton("OK") { _, _ ->
                    val newUserEmail = editText.text.toString()
                    if (newUserEmail != "") {
                        changeViewAsCharging(true)
                        userPut = UserPut(userViewModel.actualUser.name, newUserEmail, userViewModel.actualUser.type)
                        userViewModel.modifyUserData(userPut)
                        userViewModel.actualUser.email = newUserEmail
                        binding.useremailTv.text = newUserEmail
                    }
                }
                builder.setNegativeButton("Cancel") { dialogInterface, _ ->
                    dialogInterface.cancel()
                }
                builder.show()
            }

            binding.changeUserImageIb.setOnClickListener {
                selectImage()
            }

            userViewModel.isUserDataChangedSuccessfully.observe(viewLifecycleOwner) {
                changeViewAsCharging(false)
                if (it) {
                    binding.usernameTv.text = userPut.name
                    binding.useremailTv.text = userPut.email
                }
            }
        } catch (e: TimeoutException) {
            showServerErrorToast()
        } catch (e: ConnectException) {
            showServerErrorToast()
        } catch (e: SocketException) {
            showServerErrorToast()
        } catch (e: SocketTimeoutException) {
            showServerErrorToast()
        }
    }

    private fun selectImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        resultLauncherImage.launch(intent)
    }

    private fun getFileFromUri(context: Context, uri: Uri): File? {
        val inputStream = context.contentResolver.openInputStream(uri) ?: return null
        val fileName = uri.lastPathSegment ?: "file"
        val directory = context.getExternalFilesDir(null)
        val file = File(directory, fileName)
        inputStream.use { input ->
            file.outputStream().use { output ->
                input.copyTo(output)
            }
        }
        return if (file.exists()) file else null
    }

    @SuppressLint("Range")
    fun changeViewAsCharging(charging : Boolean){
        mainActivity = requireActivity() as MainActivity
        val loadingArrow = binding.loadingArrow
        if(charging){
            mainActivity.bottomNavigationView.setOnNavigationItemSelectedListener(null)

            binding.usernameTv.visibility = View.INVISIBLE
            binding.userTypeTv.visibility = View.INVISIBLE
            binding.useremailTv.visibility = View.INVISIBLE
            binding.changeUserNameIb.isClickable = false
            binding.changeUserImageIb.isClickable = false
            binding.changeUserEmailIb.isClickable = false

            loadingArrow.visibility = View.VISIBLE
            loadingArrow.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.rotate))

        }else{
            mainActivity.bottomNavigationView.setupWithNavController(mainActivity.navController)

            binding.usernameTv.visibility = View.VISIBLE
            binding.userTypeTv.visibility = View.VISIBLE
            binding.useremailTv.visibility = View.VISIBLE
            binding.changeUserNameIb.isClickable = true
            binding.changeUserImageIb.isClickable = true
            binding.changeUserEmailIb.isClickable = true

            loadingArrow.clearAnimation()
            loadingArrow.visibility = View.INVISIBLE
        }
    }

    @SuppressLint("InflateParams")
    private fun showServerErrorToast() {
        changeViewAsCharging(false)
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.custom_toast_general_error, null)
        val toast = Toast(context)
        toast.duration = Toast.LENGTH_SHORT
        toast.view = layout
        toast.show()
    }
}
package com.example.bcnpatrol.view.fragments


import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.ContentValues.TAG
import android.graphics.drawable.PictureDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.caverock.androidsvg.SVG
import com.example.bcnpatrol.model.api.Repository
import com.example.bcnpatrol.model.classes.Incidence
import com.example.bcnpatrol.model.classes.StatePut
import com.example.bcnpatrol.R
import com.example.bcnpatrol.view.activities.MainActivity
import com.example.bcnpatrol.viewModel.IncidenceViewModel
import com.example.bcnpatrol.viewModel.UserViewModel
import com.example.bcnpatrol.databinding.FragmentDetailBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.net.ConnectException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.util.concurrent.TimeoutException


class DetailFragment : Fragment() {

    private lateinit var binding: FragmentDetailBinding
    private lateinit var userViewModel: UserViewModel
    private lateinit var incidenceViewModel: IncidenceViewModel
    private lateinit var currentIncidence: Incidence
    private val isLoading = MutableLiveData<Boolean>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        userViewModel = ViewModelProvider(requireActivity())[UserViewModel::class.java]
        incidenceViewModel = ViewModelProvider(requireActivity())[IncidenceViewModel::class.java]
        binding = FragmentDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    @SuppressLint("Range")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        changeViewAsCharging(true)

        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(false)

        incidenceViewModel.isIncidenceLoaded.postValue(null)

        try {
            incidenceViewModel.currentIncidence.observe(viewLifecycleOwner) {
                currentIncidence = it

                Glide.with(requireContext())
                    .load("https://bcnpatrol-api.kirocs.es/${currentIncidence.image}")
                    .centerCrop()
                    .transform(RoundedCorners(50))
                    .into(binding.incidenceImageIv)

                if (userViewModel.actualUser.type == "2" && currentIncidence.manager != null && currentIncidence.manager.email == userViewModel.actualUser.email) {
                    binding.updateIc.visibility = View.VISIBLE
                }
                isLoading.observe(viewLifecycleOwner){ isLoaded ->
                    if (isLoaded) {
                        changeViewAsCharging(true)
                        binding.updateIc.isClickable = false
                        binding.previousPageIv.isClickable = false
                    } else {
                        changeViewAsCharging(false)
                        binding.updateIc.isClickable = true
                        binding.previousPageIv.isClickable = true
                    }
                }

                binding.incidenceName.text = currentIncidence.name
                binding.incidenceDescriptionTv.text = currentIncidence.description

                binding.incidentDirectionTv.text = currentIncidence.address

                if (currentIncidence.manager != null) {
                    binding.brigadierName.text = currentIncidence.manager.email
                }else{
                    binding.brigadierName.text = "Sin asignar"
                }

                binding.estadoTexto.text = currentIncidence.states[currentIncidence.states.lastIndex].name
                val svg = SVG.getFromString(currentIncidence.states[currentIncidence.states.lastIndex].icon_svg)
                val picture = svg.renderToPicture()
                val fotoDrawable = PictureDrawable(picture)
                binding.estadoIc.setImageDrawable(fotoDrawable)

                if(userViewModel.actualUser.type == "2"){
                    binding.reporterName.visibility = View.VISIBLE
                    binding.reporterIc.visibility = View.VISIBLE
                    binding.reporterName.text = currentIncidence.reporter.email
                }

                changeViewAsCharging(false)
            }

            binding.updateIc.setOnClickListener{
                val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_state_change, null)
                val builder = AlertDialog.Builder(context)
                builder.setTitle("Cambia el estado")
                builder.setView(dialogView)

                builder.setPositiveButton("OK") { _, _ ->
                    changeViewAsCharging(true)
                    isLoading.postValue(true)

                    val selectedItem = dialogView.findViewById<Spinner>(R.id.estados_spinner).selectedItem.toString()
                    val idState: String = when (selectedItem) {
                        "Asignada" -> "2"
                        "En Proceso" -> "3"
                        else -> "4"
                    }

                    Log.d(TAG, "Selected item: $selectedItem\nState id: $idState\nCurrent incidence id: ${currentIncidence.id}")
                    incidenceViewModel.changeState(currentIncidence.id, StatePut(idState))

                    CoroutineScope(Dispatchers.IO).launch{

                        val listSvgs: List<String>
                        val response = Repository("").getSvgs()
                        if(response.isSuccessful){
                            listSvgs = response.body()!!
                            val svgTemporal = SVG.getFromString(listSvgs[idState.toInt()-1])
                            val pictureTemporal = svgTemporal.renderToPicture()
                            val fotoDrawableTemporal = PictureDrawable(pictureTemporal)
                            binding.estadoIc.setImageDrawable(fotoDrawableTemporal)
                            binding.estadoTexto.text = selectedItem

                            changeViewAsCharging(false)
                            isLoading.postValue(false)
                        }

                    }
                }
                builder.setNegativeButton("Cancel") { dialogInterface, _ ->
                    dialogInterface.cancel()
                }
                builder.show()
            }

            binding.previousPageIv.setOnClickListener {
                if (incidenceViewModel.isMapTheCurrentFragment) {
                    findNavController().navigate(R.id.action_detailFragment_to_mapFragment)
                }
                else {
                    findNavController().navigate(R.id.action_detailFragment_to_incidentsListFragment)
                }
            }
        } catch (e: TimeoutException) {
            showServerErrorToast()
        } catch (e: ConnectException) {
            showServerErrorToast()
        } catch (e: SocketException) {
            showServerErrorToast()
        } catch (e: SocketTimeoutException) {
            showServerErrorToast()
        }
    }
    @SuppressLint("Range")
    fun changeViewAsCharging(charging : Boolean){
        val loadingArrow = binding.loadingArrow
        if (charging) {

            loadingArrow.visibility = View.VISIBLE
            loadingArrow.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.rotate))
        } else {

            loadingArrow.clearAnimation()
            loadingArrow.visibility = View.INVISIBLE
        }
    }

    @SuppressLint("InflateParams")
    private fun showServerErrorToast() {
        changeViewAsCharging(false)
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.custom_toast_general_error, null)
        val toast = Toast(context)
        toast.duration = Toast.LENGTH_SHORT
        toast.view = layout
        toast.show()
    }
}
package com.example.bcnpatrol.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bcnpatrol.model.adapter.RankingAdapter
import com.example.bcnpatrol.model.classes.UserRanking
import com.example.bcnpatrol.R
import com.example.bcnpatrol.view.activities.MainActivity
import com.example.bcnpatrol.viewModel.IncidenceViewModel
import com.example.bcnpatrol.viewModel.UserViewModel
import com.example.bcnpatrol.databinding.FragmentRankingBinding
import java.net.ConnectException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.util.concurrent.TimeoutException

class RankingFragment : Fragment() {

    private lateinit var binding: FragmentRankingBinding
    private lateinit var rankingAdapter: RankingAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var incidenceViewModel: IncidenceViewModel
    private lateinit var userViewModel: UserViewModel
    private lateinit var mainActivity: MainActivity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentRankingBinding.inflate(layoutInflater)
        incidenceViewModel = ViewModelProvider(requireActivity())[IncidenceViewModel::class.java]
        userViewModel = ViewModelProvider(requireActivity())[UserViewModel::class.java]
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        changeViewAsCharging(true)

        try {
            incidenceViewModel.getRankingList()

            incidenceViewModel.rankingList.observe(viewLifecycleOwner) { rankingList ->
                changeViewAsCharging(false)
                if (rankingList.isNotEmpty()) {
                    setUpRecyclerView(rankingList.sortedByDescending { it.count })
                }
            }
        } catch (e: TimeoutException) {
            showServerErrorToast()
        } catch (e: ConnectException) {
            showServerErrorToast()
        } catch (e: SocketException) {
            showServerErrorToast()
        } catch (e: SocketTimeoutException) {
            showServerErrorToast()
        }
    }

    private fun setUpRecyclerView(rankingList: List<UserRanking>){
        rankingAdapter = RankingAdapter(rankingList, userViewModel.actualUser)

        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = rankingAdapter
        }
    }

    @SuppressLint("Range")
    fun changeViewAsCharging(charging : Boolean) {
        mainActivity = requireActivity() as MainActivity
        val loadingArrow = binding.loadingArrow
        if(charging){
            mainActivity.bottomNavigationView.setOnNavigationItemSelectedListener(null)

            loadingArrow.visibility = View.VISIBLE
            loadingArrow.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.rotate))

        }else{
            mainActivity.bottomNavigationView.setupWithNavController(mainActivity.navController)

            loadingArrow.clearAnimation()
            loadingArrow.visibility = View.INVISIBLE
        }
    }

    @SuppressLint("InflateParams")
    private fun showServerErrorToast() {
        changeViewAsCharging(false)
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.custom_toast_general_error, null)
        val toast = Toast(context)
        toast.duration = Toast.LENGTH_SHORT
        toast.view = layout
        toast.show()
    }
}
package com.example.bcnpatrol.view.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentValues.TAG
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.bcnpatrol.R
import com.example.bcnpatrol.databinding.FragmentMapBinding
import com.example.bcnpatrol.model.classes.Incidence
import com.example.bcnpatrol.view.activities.MainActivity
import com.example.bcnpatrol.viewModel.IncidenceViewModel
import com.example.bcnpatrol.viewModel.UserViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import java.net.ConnectException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.util.concurrent.TimeoutException

const val REQUEST_CODE_LOCATION = 100
class MapFragment : Fragment(), OnMapReadyCallback {
    private lateinit var map: GoogleMap
    lateinit var binding: FragmentMapBinding
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var userViewModel: UserViewModel
    private lateinit var incidenceViewModel: IncidenceViewModel
    private lateinit var currentLocation: LatLng

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        userViewModel = ViewModelProvider(requireActivity())[UserViewModel::class.java]
        incidenceViewModel = ViewModelProvider(requireActivity())[IncidenceViewModel::class.java]
        binding = FragmentMapBinding.inflate(inflater)
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        createMap()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(true)

        incidenceViewModel.isMapTheCurrentFragment = true

        incidenceViewModel.isIncidenceAddedSuccessfully.postValue(null)

        try {
            binding.reportButton.setOnClickListener {
                goToCurrentLocation()
                if (!(::currentLocation.isInitialized)) {
                    showGeolocalizationErrorToast()
                } else {
                    val action = MapFragmentDirections.actionMapFragmentToAddNewIncidentFragment(currentLocation.latitude.toString(), currentLocation.longitude.toString(), "")
                    findNavController().navigate(action)
                }
            }
            incidenceViewModel.isIncidenceLoaded.observe(viewLifecycleOwner) {
                if (it == true) {
                    changeViewAsCharging(false)
                    findNavController().navigate(R.id.action_mapFragment_to_detailFragment)
                }
            }
        } catch (e: TimeoutException) {
            showServerErrorToast()
        } catch (e: ConnectException) {
            showServerErrorToast()
        } catch (e: SocketException) {
            showServerErrorToast()
        } catch (e: SocketTimeoutException) {
            showServerErrorToast()
        }
    }

    private fun createMap() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    override fun onMapReady(p0: GoogleMap) {
        map = p0
        map.mapType = GoogleMap.MAP_TYPE_SATELLITE
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireContext())
        enableLocation()

        goToCurrentLocation()

        val barcelona = LatLng(41.4084952,2.1654357)
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(barcelona, 13f), 3000, null)

        try {
            if (userViewModel.actualUser.type == "1") userViewModel.getUserIncidences()
            else incidenceViewModel.getAllIncidences()

            incidenceViewModel.data.observe(viewLifecycleOwner){ incidencesList ->
                if (incidencesList.isNotEmpty() && ::map.isInitialized && (userViewModel.actualUser.type == "2")) createMarkers(incidencesList.filter { it.states.last().id != "4" })
            }
            userViewModel.data.observe(viewLifecycleOwner){ incidencesList ->
                if (incidencesList.isNotEmpty() && ::map.isInitialized&& (userViewModel.actualUser.type == "1")) createMarkers(incidencesList)
            }

            map.setOnMarkerClickListener {
                changeViewAsCharging(true)
                val incidenceId = it.snippet.toString().toInt()
                incidenceViewModel.getIncidenceById(incidenceId)
                true
            }
        } catch (e: TimeoutException) {
            showServerErrorToast()
        } catch (e: ConnectException) {
            showServerErrorToast()
        } catch (e: SocketException) {
            showServerErrorToast()
        } catch (e: SocketTimeoutException) {
            showServerErrorToast()
        }
    }

    //PERMISOS
    private fun isLocationPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }
    @SuppressLint("MissingPermission")
    private fun enableLocation(){
        if(!::map.isInitialized) return
        if(isLocationPermissionGranted()){
            map.isMyLocationEnabled = true
        }
        else{
            requestLocationPermission()
        }
    }
    private fun requestLocationPermission(){
        if(ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            )){
            Toast.makeText(requireContext(), "Ves a la pantalla de permisos de l’aplicació i habilita el de Geolocalització", Toast.LENGTH_SHORT).show()
        }
        else{
            ActivityCompat.requestPermissions(requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE_LOCATION)
        }
    }

    @Deprecated("Deprecated in Java")
    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        when(requestCode){
            REQUEST_CODE_LOCATION -> if(grantResults.isNotEmpty() &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED){
                map.isMyLocationEnabled = true
            }
            else{
                Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                    Toast.LENGTH_SHORT).show()
            }
        }
    }
    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        if(!::map.isInitialized) return
        if(!isLocationPermissionGranted()){
            map.isMyLocationEnabled = false
            Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                Toast.LENGTH_SHORT).show()
        }
    }
    private fun createMarkers(listIncidences : List<Incidence>){
        Log.d(TAG, "INCIDENCES LIST SIZE: ${listIncidences.size}")
        for (incidence in listIncidences) {
            val coordinates = LatLng(incidence.lat.toDouble(),incidence.lng.toDouble())
            var color = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)
            if(userViewModel.actualUser.type == "2" && incidence.manager != null && userViewModel.actualUser.email != incidence.manager.email){
                color = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)
            }
            val myMarker = MarkerOptions()
                .position(coordinates)
                .title(incidence.name)
                .snippet(incidence.id.toString())
                .icon(color)

            map.addMarker(myMarker)
        }
    }

    @SuppressLint("Range")
    fun changeViewAsCharging(charging : Boolean) {
        val mainActivity = requireActivity() as MainActivity
        val loadingArrow = binding.loadingArrow
        if(charging){
            mainActivity.bottomNavigationView.setOnNavigationItemSelectedListener(null)
            binding.reportButton.visibility = View.INVISIBLE
            loadingArrow.visibility = View.VISIBLE
            loadingArrow.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.rotate))

        }else{
            mainActivity.bottomNavigationView.setupWithNavController(mainActivity.navController)
            binding.reportButton.visibility = View.VISIBLE
            loadingArrow.clearAnimation()
            loadingArrow.visibility = View.INVISIBLE
        }
    }

    @SuppressLint("InflateParams")
    private fun showGeolocalizationErrorToast() {
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.custom_toast_error_geolocalization, null)
        val toast = Toast(context)
        toast.duration = Toast.LENGTH_SHORT
        toast.view = layout
        toast.show()
    }

    private fun goToCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) { return }
        map.isMyLocationEnabled = true

        Log.d(TAG, "123456 goToCurrentLocation fun")
        fusedLocationProviderClient.lastLocation.addOnSuccessListener { location ->
            if (location != null) {
                currentLocation = LatLng(location.latitude, location.longitude)
            }
        }
    }


    @SuppressLint("InflateParams")
    private fun showServerErrorToast() {
        changeViewAsCharging(false)
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.custom_toast_general_error, null)
        val toast = Toast(context)
        toast.duration = Toast.LENGTH_SHORT
        toast.view = layout
        toast.show()
    }
}
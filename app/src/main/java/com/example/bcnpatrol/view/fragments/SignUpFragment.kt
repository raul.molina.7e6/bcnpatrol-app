package com.example.bcnpatrol.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.bcnpatrol.model.classes.UserRegisterAuth
import com.example.bcnpatrol.R
import com.example.bcnpatrol.viewModel.UserViewModel
import com.example.bcnpatrol.databinding.FragmentSignUpBinding
import java.net.ConnectException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.util.concurrent.TimeoutException

class SignUpFragment : Fragment() {

    private lateinit var binding: FragmentSignUpBinding
    private lateinit var userViewmodel: UserViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        userViewmodel = ViewModelProvider(requireActivity())[UserViewModel::class.java]
        binding = FragmentSignUpBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.createAccountButton.setOnClickListener {
            changeViewAsCharging(true)
            val userEmail = binding.userEmailEt.text.toString()
            val userPassword = binding.userPasswordEt.text.toString()
            val userName = binding.userNameEt.text.toString()

            if (userEmail.isEmpty() || userPassword.isEmpty() || userName.isEmpty()) {
                changeViewAsCharging(false)
                if (userEmail.isEmpty()) binding.userEmailEt.error = "El correo electronico no puede estar vacío"
                if (userPassword.isEmpty()) binding.userPasswordEt.error = "La contraseña no puede estar vacía"
                if (userName.isEmpty()) binding.userNameEt.error = "El nombre no puede estar vacío"
                return@setOnClickListener
            }
            else {
                binding.userEmailEt.error = null
                binding.userPasswordEt.error = null
                binding.userNameEt.error = null
            }

            if(!userEmail.contains('@') || !userEmail.contains('.')) {
                changeViewAsCharging(false)
                binding.userEmailEt.error = "Formato inválido de correo electronico"
                return@setOnClickListener
            }
            else {
                binding.userEmailEt.error = null
            }

            if(userPassword.length < 8) {
                changeViewAsCharging(false)
                binding.userPasswordEt.error = "La contraseña debe contener 8 caracteres como mínimo"
                return@setOnClickListener
            }
            else {
                binding.userPasswordEt.error = null
            }

            register(userName, userEmail, userPassword)
        }

        binding.alreadyHavAccountButton.setOnClickListener {
            findNavController().navigate(R.id.action_signUpFragment_to_loginFragment)
        }

        userViewmodel.isLoginSuccessful.observe(viewLifecycleOwner) {
            changeViewAsCharging(false)
            if (it == true) {
                showSuccessfullLoginToast()
                findNavController().navigate(R.id.action_signUpFragment_to_incidentsListFragment)
            }
            else if (it == false) {
                showExternalErrorToast()
            }
        }
    }

    private fun register(name: String, email: String, password: String) {
        val userAuth = UserRegisterAuth(name, email, password)
        try {
            userViewmodel.register(userAuth)
        } catch (e: TimeoutException) {
            showExternalErrorToast()
        } catch (e: ConnectException) {
            showExternalErrorToast()
        } catch (e: SocketException) {
            showExternalErrorToast()
        } catch (e: SocketTimeoutException) {
            showExternalErrorToast()
        }
    }

    @SuppressLint("InflateParams")
    private fun showSuccessfullLoginToast() {
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.custom_toast_ok, null)
        val toast = Toast(context)
        toast.duration = Toast.LENGTH_SHORT
        toast.view = layout
        toast.show()
    }

    @SuppressLint("InflateParams")
    private fun showExternalErrorToast() {
        changeViewAsCharging(false)
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.custom_toast_general_error, null)
        val toast = Toast(context)
        toast.duration = Toast.LENGTH_SHORT
        toast.view = layout
        toast.show()
    }

    @SuppressLint("Range")
    fun changeViewAsCharging(charging : Boolean){
        val loadingArrow = binding.loadingArrow
        if(charging){
            binding.createAccountButton.isClickable = false
            binding.alreadyHavAccountButton.isClickable = false

            loadingArrow.visibility = View.VISIBLE
            loadingArrow.startAnimation(AnimationUtils.loadAnimation(requireContext(), com.example.bcnpatrol.R.anim.rotate))

        }else{

            binding.createAccountButton.isClickable = true
            binding.alreadyHavAccountButton.isClickable = true

            loadingArrow.clearAnimation()
            loadingArrow.visibility = View.INVISIBLE
        }

    }
}
package com.example.bcnpatrol.view.fragments

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bcnpatrol.model.adapter.IncidenceAdapter
import com.example.bcnpatrol.model.adapter.MyOnClickListener
import com.example.bcnpatrol.model.classes.Incidence
import com.example.bcnpatrol.R
import com.example.bcnpatrol.view.activities.MainActivity
import com.example.bcnpatrol.viewModel.IncidenceViewModel
import com.example.bcnpatrol.viewModel.UserViewModel
import com.example.bcnpatrol.databinding.FragmentIncidentsListBinding
import com.google.android.material.badge.BadgeDrawable
import java.net.ConnectException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.util.concurrent.TimeoutException

class IncidentsListFragment : Fragment(), MyOnClickListener {
    private lateinit var incidenceAdapter: IncidenceAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentIncidentsListBinding
    private lateinit var userViewModel: UserViewModel
    private lateinit var incidenceViewModel: IncidenceViewModel
    private lateinit var badge: BadgeDrawable
    private var selectedItem = "Sin filtro"
    private lateinit var mainActivity: MainActivity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        userViewModel = ViewModelProvider(requireActivity())[UserViewModel::class.java]
        incidenceViewModel = ViewModelProvider(requireActivity())[IncidenceViewModel::class.java]
        binding = FragmentIncidentsListBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        changeViewAsCharging(true)

        setUpRecyclerView(listOf())

        mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(true)

        incidenceViewModel.isMapTheCurrentFragment = false

        badge = mainActivity.bottomNavigationView.getOrCreateBadge(R.id.incidentsListFragment)
        badge.backgroundColor = Color.parseColor("#ffe294")
        badge.badgeTextColor = Color.BLACK
        badge.number = 0

        userViewModel.isLoginSuccessful.postValue(null)

        try {
            if (userViewModel.actualUser.type == "2") {
                val categories = arrayOf("Sin filtro", "Tus incidencias")
                binding.filterSpinner.adapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, categories)
            }

            binding.usernameTv.text = userViewModel.actualUser.name

            binding.repeatIb.setOnClickListener {
                showIncidencesByFilter()
            }

            val mySpinner =  binding.filterSpinner

            mySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                    // Aquí se ejecutará el código cuando se seleccione un elemento en el Spinner
                    selectedItem = parent.getItemAtPosition(position).toString()
                    showIncidencesByFilter()
                }
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    TODO("Not yet implemented")
                }
            }
        } catch (e: TimeoutException) {
            showServerErrorToast()
        } catch (e: ConnectException) {
            showServerErrorToast()
        } catch (e: SocketException) {
            showServerErrorToast()
        } catch (e: SocketTimeoutException) {
            showServerErrorToast()
        }
    }

    private fun showIncidencesByFilter() {
        if (userViewModel.actualUser.type == "2") {
            incidenceViewModel.getAllIncidences()
            incidenceViewModel.data.observe(viewLifecycleOwner){ incidentsList ->
                changeViewAsCharging(false)
                if (incidentsList.isEmpty()) {
                    binding.recyclerView.visibility = View.INVISIBLE
                    binding.emptyView.visibility = View.VISIBLE
                } else {
                    binding.recyclerView.visibility = View.VISIBLE
                    binding.emptyView.visibility = View.INVISIBLE
                }
                when (selectedItem) {
                    "Sin filtro"-> {
                        setUpRecyclerView(incidentsList.sortedBy { it.states[it.states.lastIndex].id  })
                    }
                    "Tus incidencias" -> {
                        setUpRecyclerView(incidentsList.filter {
                            it.manager != null && it.manager.email == userViewModel.actualUser.email
                        }.sortedBy { it.states[it.states.lastIndex].id })
                    }

                }
            }

        } else {
            userViewModel.getUserIncidences()
            userViewModel.data.observe(viewLifecycleOwner){ incidentsList ->
                changeViewAsCharging(false)
                var lista = listOf<Incidence>()
                when (selectedItem) {
                    "Sin filtro"-> {
                        setUpRecyclerView(incidentsList.sortedBy { it.states[it.states.lastIndex].id  })
                        lista = incidentsList
                    }
                    "Registrada"-> {
                        setUpRecyclerView(incidentsList.filter { it.states[it.states.lastIndex].id == "1" })
                        lista = incidentsList.filter { it.states[it.states.lastIndex].id == "1" }
                    }
                    "Asignada" -> {
                        setUpRecyclerView(incidentsList.filter { it.states[it.states.lastIndex].id == "2" })
                        lista = incidentsList.filter { it.states[it.states.lastIndex].id == "2" }
                    }
                    "En Proceso" -> {
                        setUpRecyclerView(incidentsList.filter { it.states[it.states.lastIndex].id == "3" })
                        lista = incidentsList.filter { it.states[it.states.lastIndex].id == "3" }
                    }
                    "Cerrada" -> {
                        setUpRecyclerView(incidentsList.filter { it.states[it.states.lastIndex].id == "4" })
                        lista = incidentsList.filter { it.states[it.states.lastIndex].id == "4" }
                    }
                }
                if (lista.isEmpty()) {
                    binding.recyclerView.visibility = View.INVISIBLE
                    binding.emptyView.visibility = View.VISIBLE
                } else {
                    binding.recyclerView.visibility = View.VISIBLE
                    binding.emptyView.visibility = View.INVISIBLE
                }
            }
        }
    }

    private fun setUpRecyclerView(incidencesList: List<Incidence>){
        incidenceAdapter = IncidenceAdapter(incidencesList, this)

        if (::badge.isInitialized) {
            if (userViewModel.actualUser.type == "1") {
                val incidencesSize = (incidencesList.filter { it.states.last().id != "4" && it.reporter != null && it.reporter.email == userViewModel.actualUser.email}).size
                if (incidencesSize != 0) {
                    badge.number = incidencesSize
                }
            }
            else {
                val incidencesSize = (incidencesList.filter { it.states.last().id != "4" && it.manager != null && it.manager.email == userViewModel.actualUser.email}).size
                if (incidencesSize != 0) {
                    badge.number = incidencesSize

                }
            }
        }

        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = incidenceAdapter
        }

    }

    override fun onClick(incidence: Incidence) {
        incidenceViewModel.currentIncidence.postValue(incidence)
        findNavController().navigate(R.id.action_incidentsListFragment_to_detailFragment)
    }

    override fun onStart() {
        super.onStart()

        try {
            if (userViewModel.actualUser.type == "2") {
                incidenceViewModel.getAllIncidences()
                binding.misIncidenciasTv.text = "Incidencias"
            }
            else if (userViewModel.actualUser.type == "1") {
                userViewModel.getUserIncidences()
                binding.misIncidenciasTv.text = "Mis incidencias"
            }
        } catch (e: TimeoutException) {
            showServerErrorToast()
        } catch (e: ConnectException) {
            showServerErrorToast()
        } catch (e: SocketException) {
            showServerErrorToast()
        } catch (e: SocketTimeoutException) {
            showServerErrorToast()
        }
    }

    @SuppressLint("Range")
    fun changeViewAsCharging(charging : Boolean){
        mainActivity = requireActivity() as MainActivity
        val loadingArrow = binding.loadingArrow
        if(charging){
            mainActivity.bottomNavigationView.setOnNavigationItemSelectedListener(null)
            binding.repeatIb.visibility = View.GONE
            loadingArrow.visibility = View.VISIBLE
            loadingArrow.startAnimation(AnimationUtils.loadAnimation(requireContext(), com.example.bcnpatrol.R.anim.rotate))

        }else{
            mainActivity.bottomNavigationView.setupWithNavController(mainActivity.navController)
            binding.repeatIb.visibility = View.VISIBLE
            loadingArrow.clearAnimation()
            loadingArrow.visibility = View.INVISIBLE
        }

    }

    @SuppressLint("InflateParams")
    private fun showServerErrorToast() {
        changeViewAsCharging(false)
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.custom_toast_general_error, null)
        val toast = Toast(context)
        toast.duration = Toast.LENGTH_SHORT
        toast.view = layout
        toast.show()
    }
}
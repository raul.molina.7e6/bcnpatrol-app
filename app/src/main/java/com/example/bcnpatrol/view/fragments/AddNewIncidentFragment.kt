package com.example.bcnpatrol.view.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.ActivityInfo
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.edit
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.bcnpatrol.R
import com.example.bcnpatrol.databinding.FragmentAddNewIncidentBinding
import com.example.bcnpatrol.model.classes.IncidencePost
import com.example.bcnpatrol.view.activities.MainActivity
import com.example.bcnpatrol.viewModel.IncidenceViewModel
import com.example.bcnpatrol.viewModel.UserViewModel
import java.io.File
import java.net.ConnectException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.util.concurrent.TimeoutException

class AddNewIncidentFragment : Fragment() {
    lateinit var binding: FragmentAddNewIncidentBinding
    private lateinit var userViewmodel: UserViewModel
    private lateinit var incidenceViewModel: IncidenceViewModel
    private var uri = ""
    var lat = ""
    private var lon = ""
    private lateinit var myPreferences: SharedPreferences
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val mainActivity = requireActivity() as MainActivity
        mainActivity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        userViewmodel = ViewModelProvider(requireActivity())[UserViewModel::class.java]
        incidenceViewModel = ViewModelProvider(requireActivity())[IncidenceViewModel::class.java]
        binding = FragmentAddNewIncidentBinding.inflate(inflater, container, false)
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        changeViewAsCharging(true)

        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(false)

        myPreferences = requireActivity().getSharedPreferences("MySharedPreferences", Context.MODE_PRIVATE)

        setupForm()

        lat = arguments?.getString("lat").toString()
        lon = arguments?.getString("lon").toString()
        uri = arguments?.getString("uri").toString()

        if (uri != "") {
            Glide.with(requireContext())
                .load(uri.toUri())
                .centerCrop()
                .transform(RoundedCorners(50))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.incidentImageIv)
        }

        try {

            userViewmodel.getCategoriesForSpinner()
            var categories: Array<String>

            userViewmodel.categories.observe(viewLifecycleOwner){ info ->
                changeViewAsCharging(false)
                categories = info!!.toTypedArray()
                println(categories)
                binding.categoriesSpinner.adapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, categories)
            }

            binding.cameraIconIv.setOnClickListener {
                rememberInfo(binding.incidentNameEt.text.toString(), binding.incidentDescriptionEt.text.toString(), uri, true, lat, lon)
                val action = AddNewIncidentFragmentDirections.actionAddNewIncidentFragmentToCamaraFragment(lat, lon)
                findNavController().navigate(action)
            }
            binding.addbutton.setOnClickListener{
                changeViewAsCharging(true)
                rememberInfo(binding.incidentNameEt.text.toString(), binding.incidentDescriptionEt.text.toString(), uri, false, lat, lon)
                //AÑADIR INCIDENCIA
                val incidence = IncidencePost(binding.incidentNameEt.text.toString(), uri, binding.incidentDescriptionEt.text.toString(), lat, lon, userViewmodel.actualUser.email, binding.categoriesSpinner.selectedItem.toString())
                incidenceViewModel.addIncidence(incidence, getFileFromUri(requireContext(), uri.toUri())!!)
            }
            binding.cancelbutton.setOnClickListener {
                rememberInfo(binding.incidentNameEt.text.toString(), binding.incidentDescriptionEt.text.toString(), uri, false, lat, lon)
                val action = AddNewIncidentFragmentDirections.actionAddNewIncidentFragmentToMapFragment()
                findNavController().navigate(action)
            }

            incidenceViewModel.isIncidenceAddedSuccessfully.observe(viewLifecycleOwner) {
                changeViewAsCharging(false)
                if (it == true) {
                    showIncidenceAddedToast()
                    findNavController().navigate(R.id.action_addNewIncidentFragment_to_mapFragment)
                }
                else if (it == false) {
                    showIncidenceExistsErrorToast()
                    findNavController().navigate(R.id.action_addNewIncidentFragment_to_mapFragment)
                }
            }
        } catch (e: TimeoutException) {
            showServerErrorToast()
        } catch (e: ConnectException) {
            showServerErrorToast()
        } catch (e: SocketException) {
            showServerErrorToast()
        } catch (e: SocketTimeoutException) {
            showServerErrorToast()
        }
    }

    private fun rememberInfo(name: String, desc: String, uri: String, aFoto: Boolean, lat:String, lon:String) {
        if(aFoto){
            myPreferences.edit {
                putString("name", name)
                putString("desc", desc)
                putString("uri", uri)
                putString("lat", lat)
                putString("lon", lon)
                apply()
            }
        }else{
            myPreferences.edit {
                putString("name", "")
                putString("desc", "")
                putString("uri", "")
                putString("lat", "")
                putString("lon", "")
                apply()
            }
        }

    }
    private fun setupForm() {
        val name = myPreferences.getString("name", "")
        val desc = myPreferences.getString("desc", "")
        uri = myPreferences.getString("uri", "").toString()
        lat = myPreferences.getString("uri", "").toString()
        lon = myPreferences.getString("uri", "").toString()
        if (name != null) {
            if(name.isNotEmpty()){
                binding.incidentNameEt.setText(name)
                binding.incidentDescriptionEt.setText(desc)
            }
        }
    }

    private fun getFileFromUri(context: Context, uri: Uri): File? {
        val inputStream = context.contentResolver.openInputStream(uri) ?: return null
        val fileName = uri.lastPathSegment ?: "file"
        val directory = context.getExternalFilesDir(null)
        val file = File(directory, fileName)
        inputStream.use { input ->
            file.outputStream().use { output ->
                input.copyTo(output)
            }
        }
        return if (file.exists()) file else null
    }

    @SuppressLint("InflateParams")
    private fun showIncidenceExistsErrorToast() {
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.custom_toast_error_incidence_exists, null)
        val toast = Toast(context)
        toast.duration = Toast.LENGTH_SHORT
        toast.view = layout
        toast.show()
    }

    @SuppressLint("InflateParams")
    private fun showIncidenceAddedToast() {
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.custom_toast_incidence_added_successfully, null)
        val toast = Toast(context)
        toast.duration = Toast.LENGTH_SHORT
        toast.view = layout
        toast.show()
    }

    @SuppressLint("InflateParams")
    private fun showServerErrorToast() {
        changeViewAsCharging(false)
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.custom_toast_general_error, null)
        val toast = Toast(context)
        toast.duration = Toast.LENGTH_SHORT
        toast.view = layout
        toast.show()
    }

    @SuppressLint("Range")
    fun changeViewAsCharging(charging : Boolean){
        val loadingArrow = binding.loadingArrow
        if(charging){
            binding.addbutton.isClickable = false
            binding.cancelbutton.isClickable = false

            loadingArrow.visibility = View.VISIBLE
            loadingArrow.startAnimation(AnimationUtils.loadAnimation(requireContext(), com.example.bcnpatrol.R.anim.rotate))

        }else{
            binding.addbutton.isClickable = true
            binding.cancelbutton.isClickable = true

            loadingArrow.clearAnimation()
            loadingArrow.visibility = View.INVISIBLE
        }
    }
}
package com.example.bcnpatrol.model.classes

import okhttp3.Interceptor
import okhttp3.Response

class AuthHeader(private val authToken: String) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val authenticatedRequest = request.newBuilder()
            .header("Authorization", "Bearer $authToken")
            .build()
        return chain.proceed(authenticatedRequest)
    }
}
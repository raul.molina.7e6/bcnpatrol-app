package com.example.bcnpatrol.model.classes

data class Data(
    val address: String,
    val category_id: String,
    val completed: Boolean,
    val created_at: String,
    val description: String,
    val id: Int,
    val image: String,
    val lat: String,
    val lng: String,
    val name: String,
    val reporter: String,
    val updated_at: String
)
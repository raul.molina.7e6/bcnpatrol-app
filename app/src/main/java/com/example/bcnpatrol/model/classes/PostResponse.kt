package com.example.bcnpatrol.model.classes

data class PostResponse(
    val `data`: Data,
    val msg: String,
    val status: String
)
package com.example.bcnpatrol.model.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.bcnpatrol.model.classes.User
import com.example.bcnpatrol.model.classes.UserRanking
import com.example.bcnpatrol.R
import com.example.bcnpatrol.databinding.ItemRankBinding

class RankingAdapter(private val ranking: List<UserRanking>, private val actualUser: User): RecyclerView.Adapter<RankingAdapter.ViewHolder>() {

    private lateinit var context: Context

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val binding = ItemRankBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_rank, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return ranking.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val userRanking = ranking[position]
        with(holder){
            if (userRanking.email == actualUser.email) {
                binding.rankCardView.setCardBackgroundColor(Color.parseColor("#d14040"))
                binding.userNameTv.setTextColor(Color.WHITE)
                binding.userReportsTv.setTextColor(Color.WHITE)
                binding.userPositionTv.setTextColor(Color.WHITE)
            }

            if (userRanking.name.length > 12) {
                binding.userNameTv.text = userRanking.name.substring(0, 12)
            }
            else {
                binding.userNameTv.text = userRanking.name
            }
            binding.userPositionTv.text = (position+1).toString()
            binding.userReportsTv.text = userRanking.count.toString()

            Glide.with(context)
                .load("https://bcnpatrol-api.kirocs.es/${userRanking.image}")
                .transform(RoundedCorners(300))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.userImageIv)

            if("https://bcnpatrol-api.kirocs.es/${userRanking.image}" != "https://bcnpatrol-api.kirocs.es/storage/users/default.png"){
                binding.userImageIv.rotation = -90f
            }
        }
    }
}
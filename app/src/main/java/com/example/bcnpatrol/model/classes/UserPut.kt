package com.example.bcnpatrol.model.classes

data class UserPut(val name: String, val email: String, val type: String)

package com.example.bcnpatrol.model.adapter

import com.example.bcnpatrol.model.classes.Incidence

interface MyOnClickListener {
    fun onClick(incidence: Incidence)
}
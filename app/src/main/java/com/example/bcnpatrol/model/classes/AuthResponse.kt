package com.example.bcnpatrol.model.classes

data class AuthResponse(
    val authorization: Authorization,
    val status: String,
    val user: User
)
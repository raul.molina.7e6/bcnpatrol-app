package com.example.bcnpatrol.model.classes

data class Category(
    val id: Int,
    val name: String
)
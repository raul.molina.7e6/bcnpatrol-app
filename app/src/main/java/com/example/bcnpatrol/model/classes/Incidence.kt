package com.example.bcnpatrol.model.classes


data class Incidence(
    val id: Int,
    val name: String,
    val image: String,
    val description: String,
    val address: String,
    val lat: String,
    val lng: String,
    val completed: Boolean,
    val reporter: User,
    val manager: User,
    val category_id: Int,
    val current_state: Int,
    val created_at: String,
    val updated_at: String,
    val category_name: String,
    val states: List<State>,
    val category: Category
)
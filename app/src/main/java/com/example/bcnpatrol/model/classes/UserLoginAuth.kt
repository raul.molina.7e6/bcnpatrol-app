package com.example.bcnpatrol.model.classes

data class UserLoginAuth(val email: String, val password: String)

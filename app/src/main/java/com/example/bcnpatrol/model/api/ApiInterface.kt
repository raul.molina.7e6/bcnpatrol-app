package com.example.bcnpatrol.model.api

import com.example.bcnpatrol.model.classes.AuthHeader
import com.example.bcnpatrol.model.classes.AuthResponse
import com.example.bcnpatrol.model.classes.Email
import com.example.bcnpatrol.model.classes.Incidence
import com.example.bcnpatrol.model.classes.PostResponse
import com.example.bcnpatrol.model.classes.StatePut
import com.example.bcnpatrol.model.classes.UserLoginAuth
import com.example.bcnpatrol.model.classes.UserPut
import com.example.bcnpatrol.model.classes.UserRanking
import com.example.bcnpatrol.model.classes.UserRegisterAuth
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Url

interface ApiInterface {
    @GET()
    suspend fun getUserIncidences(@Url url: String): Response<List<Incidence>>

    @Multipart
    @POST("incidences")
    suspend fun addIncidence(
        @Part ("name") name: RequestBody,
        @Part ("description") description: RequestBody,
        @Part ("lat") lat: RequestBody,
        @Part ("lng") lng: RequestBody,
        @Part ("reporter") reporter: RequestBody,
        @Part ("category") category: RequestBody,
        @Part image: MultipartBody.Part
    ): Response<PostResponse>

    @POST("login")
    suspend fun login(@Body user: UserLoginAuth): Response<AuthResponse>
    @POST("register")
    suspend fun register(@Body user: UserRegisterAuth): Response<AuthResponse>
    @DELETE()
    suspend fun deleteIncidence(@Url url: String): Response<Boolean>
    @GET("incidences")
    suspend fun getAllIncidences(): Response<List<Incidence>>
    @GET()
    suspend fun getCategoriesForSpinner(@Url url: String): Response<List<String>?>
    @GET("incidences/{id}")
    suspend fun getIncidenceById(@Path("id") id: Int): Response<Incidence>
    @POST("users/request-password-reset")
    suspend fun recoverPassword(@Body email: Email): Response<ResponseBody>
    @PUT("users/{email}")
    suspend fun changeUserEmail(@Path("email") email: String, @Body user: UserPut): Response<ResponseBody>
    @Multipart
    @POST("users/{email}/image")
    suspend fun modifyUserImage(@Path("email") email: String,
                                @Part image: MultipartBody.Part): Response<ResponseBody>
    @GET("stats/all-time-ranking")
    suspend fun getRanking(): Response<List<UserRanking>>

    @PUT("incidences/{id}/state")
    suspend fun changeState(@Path("id") id: Int, @Body newState: StatePut): Response<ResponseBody>

    @GET()
    suspend fun getSvgs(@Url url: String): Response<List<String>>

    companion object {
        private const val BASE_URL = "https://bcnpatrol-api.kirocs.es/api/"
        fun create(authToken: String): ApiInterface {

            val client = OkHttpClient.Builder()
                .addInterceptor(AuthHeader(authToken))
                .build()

            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }
}
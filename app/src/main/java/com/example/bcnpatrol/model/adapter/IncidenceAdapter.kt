package com.example.bcnpatrol.model.adapter

import android.content.Context
import android.graphics.drawable.PictureDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.caverock.androidsvg.SVG
import com.example.bcnpatrol.model.classes.Incidence
import com.example.bcnpatrol.R
import com.example.bcnpatrol.databinding.ItemIncidentBinding

class IncidenceAdapter(private val incidences: List<Incidence>, private var listener: MyOnClickListener): RecyclerView.Adapter<IncidenceAdapter.ViewHolder>() {
    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemIncidentBinding.bind(view)
        fun setListener(incidence: Incidence){
            binding.root.setOnClickListener {
                listener.onClick(incidence)
            }
        }
    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_incident, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return incidences.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val incidence = incidences[position]
        with(holder){
            setListener(incidence)

            if (incidence.name.length > 15) {
                binding.incidentTitleTv.text = incidence.name.substring(0, 15)
            } else {
                binding.incidentTitleTv.text = incidence.name
            }

            if (incidence.address.length > 15) {
                binding.incidentDirectionTv.text = incidence.address.substring(0, 15)
            } else {
                binding.incidentDirectionTv.text = incidence.address
            }

           // if (incidence.manager.email == actualUser.email) {
            //                binding.incidentCardView.setCardBackgroundColor(Color.BLUE)
            //            }

            val svg = SVG.getFromString(incidence.states[incidence.states.lastIndex].icon_svg)
            val picture = svg.renderToPicture()
            val fotoDrawable = PictureDrawable(picture)
            binding.estadoIcadapter.setImageDrawable(fotoDrawable)

            //when (incidence.current_state) {
            //                1 -> {
            //
            //                }
            //                2 -> {
            //
            //                }
            //                3 -> {
            //
            //                }
            //                4 -> {
            //                    binding.incidentCardView.setCardBackgroundColor(Color.parseColor("#002a8c"))
            //                    binding.incidentTitleTv.setTextColor(Color.WHITE)
            //                    binding.incidentDirectionTv.setTextColor(Color.WHITE)
            //                }
            //            }
            Glide.with(context)
                .load("https://bcnpatrol-api.kirocs.es/${incidence.image}")
                .centerCrop()
                .into(binding.incidentIv);
        }
    }
}
package com.example.bcnpatrol.model.api

import com.example.bcnpatrol.model.classes.Email
import com.example.bcnpatrol.model.classes.StatePut
import com.example.bcnpatrol.model.classes.UserLoginAuth
import com.example.bcnpatrol.model.classes.UserPut
import com.example.bcnpatrol.model.classes.UserRegisterAuth
import okhttp3.MultipartBody
import okhttp3.RequestBody

class Repository(authToken: String) {
    private val apiInterface = ApiInterface.create(authToken)

    //USER

    suspend fun getUserIncidences(emailUser: String) = apiInterface.getUserIncidences("citizens/incidences/$emailUser")
    suspend fun getAllIncidences() = apiInterface.getAllIncidences()

    suspend fun addIncidence(name: RequestBody, description: RequestBody, lat: RequestBody,  lng: RequestBody, reporter: RequestBody, category: RequestBody, image: MultipartBody.Part) = apiInterface.addIncidence(name, description, lat, lng, reporter, category, image)

    suspend fun modifyUserImage(email: String, image: MultipartBody.Part) = apiInterface.modifyUserImage(email, image)

    suspend fun getCategoriesForSpinner() = apiInterface.getCategoriesForSpinner("categories")

    suspend fun getRanking() = apiInterface.getRanking()

    suspend fun deleteIncidence(id: String) = apiInterface.deleteIncidence("incidences/$id")

    suspend fun login(user: UserLoginAuth) = apiInterface.login(user)
    suspend fun register(user: UserRegisterAuth) = apiInterface.register(user)

    suspend fun recoverPassword(email: Email) = apiInterface.recoverPassword(email)

    suspend fun changeUserEmail(email: String, user: UserPut) = apiInterface.changeUserEmail(email, user)


    //BRIGADIER
    suspend fun getIncidenceBrigadier(id: String) = apiInterface.getUserIncidences("incidences")

    suspend fun getListOfResolvedIncidencesBrigadier(emailBrigadier: String) = apiInterface.getUserIncidences("incidences/$emailBrigadier")

    // INCIDENCE
    suspend fun changeState(id: Int, state: StatePut) = apiInterface.changeState(id,state)

    suspend fun getIncidenceById(id: Int) = apiInterface.getIncidenceById(id)

    suspend fun getSvgs() = apiInterface.getSvgs("states/svgs")

}
package com.example.bcnpatrol.model.classes

data class State(
    val id:String,
    val name: String,
    val color: String,
    val icon_class: String,
    val icon_svg: String,
    val pivot: Pivot
)
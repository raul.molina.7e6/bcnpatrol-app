package com.example.bcnpatrol.model.classes

data class User(
    val created_at: String,
    var email: String,
    var name: String,
    val type: String,
    var image: String,
    val updated_at: String
)
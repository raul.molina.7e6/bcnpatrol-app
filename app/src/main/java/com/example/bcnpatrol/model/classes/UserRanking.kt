package com.example.bcnpatrol.model.classes

data class UserRanking(
    val created_at: String,
    var email: String,
    var name: String,
    val type: String,
    val image: String,
    val updated_at: String,
    val count: Int
)
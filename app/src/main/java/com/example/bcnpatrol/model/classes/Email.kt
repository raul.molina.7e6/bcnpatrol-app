package com.example.bcnpatrol.model.classes

data class Email (val email: String)
package com.example.bcnpatrol.model.classes

data class IncidencePost(
    val name: String,
    val image: String,
    val description: String,
    val lat: String,
    val lng: String,
    val reporter: String,
    val category: String
)

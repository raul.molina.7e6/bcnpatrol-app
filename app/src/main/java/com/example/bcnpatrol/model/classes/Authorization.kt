package com.example.bcnpatrol.model.classes

data class Authorization(
    val token: String,
    val type: String
)
package com.example.bcnpatrol.model.classes

data class UserRegisterAuth(val name: String, val email: String, val password: String)

package com.example.bcnpatrol.model.classes

data class Pivot(
    val incidence_id: Int,
    val state_id: Int,
    val created_at: String
)

Contents
========

 * [Título](#Título)
 * [Descripción](#Descripción)
 * [Instalación](#Instalación)
 * [Uso](#Uso)
 * [Arquitectura](#Arquitectura)
 * [Quieres contribuir?](#Quieres-contribuir)

### Título
---

BCN Patrol (APP)

### Descripción
---

BCN Patrol es un producto desarrollado para el ayuntamiento de Barcelona, enfocado para el seguimiento de las incidencias en la vía pública. Desde esta los ciudadanos pueden reportar daños a inmobiliario urbano u otros problemas que puedan surgir como comportamientos incívicos.

Además contamos con un panel de administración de las mismas para que puedan ser asignadas a los diferentes brigadistas del ayuntamiento para así favorecer su rápida resolución.

### Instalación
---

Simplemente copia el proyecto desde Android Studio.

```bash
$ git clone https://gitlab.com/raul.molina.7e6/bcnpatrol-app.git
```

### Uso
---

Primero de todo create una cuenta y despues inicia sesión con la cuenta creada.

Reporta incidencias desde tu ubicación actual desde la pantalla de mapa.

Revisa tus incidencias reportadas des del mapa o des del listado de incidencias.

Accede a tu perfil y cambia tus datos actuales.

### Arquitectura
---

La arquitectura del programa está hecha con patrón MVVM.

`model` : Representa los datos y la lógica de negocio de la aplicación. El modelo puede ser de la API y de la aplicación.

`view` : Representa la interfaz de usuario de la aplicación. La vista es responsable de mostrar los datos y recibir la entrada del usuario.

`viewmodel` : Actúa como intermediario entre la vista y el modelo. El ViewModel es responsable de exponer los datos del modelo a la vista y manejar las acciones del usuario.

### Quieres contribuir?
---

Revisa el documento `LICENSE`
